package android.olivasa.crystalball;

/**
 * Created by Student on 8/26/2015.
 */
public class Predictions {

    public static Predictions predictions;
    private String[] answers;

    private Predictions() {
        answers = new String[] {
                "Your wishes will come true.",
                "Your wishes will NEVER come true, scrub."
        };

    }

    public static Predictions get(){
        if (predictions == null) {
            predictions = new Predictions();
        }
        return predictions;
    }

    public String getPredictions() {
        return answers[0];
    }
}
